import {Index as Dashboard} from "./components/dashboard";
import "./App.scss";
import 'antd/dist/antd.css';
function App() {
    return (
        <div className="container">
            <Dashboard/>
        </div>
    );
}

export default App;
