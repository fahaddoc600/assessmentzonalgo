import React from 'react';
import {ClockCircleOutlined} from "@ant-design/icons";
import {priceWithCommas, STRINGS} from "../../../utils/base";

const Card = (props) => {
    let {data: {price, images, transportation, name, address, description, check_out_time, check_in_time, facilities,_id}, onSelect,selected_id} = props;

    const renderIconAcc = (type) => {
        switch (type) {
            case "airports":
                return <i className="ic-air-plane"/>
            case "rails":
                return <i className="ic-train"/>
            case "subways":
                return <i className="ic-subway"/>
            case "cruises":
                return <i className="ic-ship"/>
            default :
                return <i className="ic-air-plane"/>
        }
    }

    return (
        <div className={`card ${selected_id === _id && "active"}`} onClick={() => onSelect({facilities,_id})}>
            <div className="lft">
                <div className="lg">
                    <img alt={"#"} loading={"lazy"} src={images[0] === undefined ? images[3] : images[0]}/>
                </div>
            </div>
            <div className="rt">
                <div className="hd">
                    <div className="txt">
                        {name}
                    </div>
                    <div className="tags">
                        <div className="tag">
                            5 Star
                        </div>
                        <div className="tag" style={{backgroundColor: "#f0983d"}}>
                            Hotel
                        </div>
                    </div>
                </div>
                <div className="sec">
                    <div className="sep" style={{justifyContent: "space-between"}}>
                        <div className="str">
                            <div className="txt">
                                {address} -
                                <span>Show on map</span>
                            </div>
                        </div>
                        <div className="mid">
                            {
                                transportation?.map((tr, index) => {
                                    return (
                                        <div key={index} className="icon-box">
                                            <div className="ic">
                                                {
                                                    renderIconAcc(tr)
                                                }
                                            </div>
                                            <div className="txt">
                                                {tr}
                                            </div>
                                        </div>
                                    )
                                })
                            }

                        </div>
                        <div className="end">
                            <p>{description[0]}</p>
                            {/*{*/}
                            {/*    description?.map((d, index) => {*/}
                            {/*        return (*/}
                            {/*            <p key={index}>{d}</p>*/}
                            {/*        )*/}
                            {/*    })*/}
                            {/*}*/}

                        </div>
                    </div>
                    <div className="sep">
                        <div className="end">
                            <div className="txt">Check-In</div>
                            <div className="sub-txt">{check_in_time === "" && "--"}</div>
                        </div>
                    </div>
                    <div className="sep">
                        <div className="end">
                            <div className="txt">Check-Out</div>
                            <div className="sub-txt">{check_out_time === "" && "--"}</div>
                        </div>
                    </div>
                    <div className="sep">
                        <div className="end">
                            <div className="ic">
                                <ClockCircleOutlined style={{
                                    color: `${STRINGS.COLORS.TEXT}`
                                }}/>
                            </div>
                            <div className="sub-txt">00:00 - 00:00</div>
                        </div>
                    </div>
                    <div className="sep">
                        <div className="str" style={{
                            justifyContent: "center",
                            margin: "0.5rem 0"
                        }}>
                            <div className="price">
                                <div className="price-txt">
                                    Price start fr:
                                    <span>Rs {priceWithCommas(price)}</span>
                                </div>
                                <span>Taxes & Fees included</span>
                            </div>
                        </div>
                        <div className="mid" style={{
                            justifyContent: "center"
                        }}>
                            <div className="ac">
                                <button className="btn">Reserve</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Card;