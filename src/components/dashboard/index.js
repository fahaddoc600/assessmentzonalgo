import React, {useState} from 'react';
import SwitchButton from "../sharedComponent/switchButton";
import {CloseOutlined} from "@ant-design/icons";
import ShutterButton from "../sharedComponent/shutterButton";
import RangeSlider from "../sharedComponent/rangeSlider";
import Card from "./items/Card";
import {store} from "../../store/store";
import {Divider} from "antd";
import CheckBox from "../sharedComponent/checkbox";

const defaultState = {
    facilities: [],
    selected_id: null
}

export const Index = () => {
    const [init, setInit] = useState(defaultState);
    let {facilities,selected_id} = init;

    const onSelect = ({facilities, _id}) => {
        setInit({
            ...init,
            facilities: facilities,
            selected_id: _id
        })
    };

    return (
        <div className="main-section">
            <div className="lft-cont">
                <div className="content">
                    <div className="cls">
                        <div className="btn">
                            <CloseOutlined/>
                        </div>
                    </div>
                    <div className="card-sml">
                        <div className="hd">
                            <div className="txt">
                                Our abc
                            </div>
                            <div className="sub-txt">
                                Now's a great time to book
                            </div>
                        </div>
                        <div className="sc">
                            <div className="txt">
                                kajskaskasjkaj
                            </div>
                        </div>
                        <div className="ft">
                            <div className="txt">Price Alert</div>
                            <div className="ac">
                                <SwitchButton/>
                            </div>
                        </div>
                    </div>

                    <div className="pr-sec">
                        <div className="hd">
                            <div className="b-txt">
                                Price
                            </div>
                            <div className="ac">
                                <ShutterButton/>
                            </div>
                        </div>
                        <div className="bd">
                            <div className="sl">
                                <RangeSlider/>
                            </div>
                            <div className="row">
                                <div className="txt">
                                    from
                                    <span>Rs 20000</span>
                                    to
                                    <span>Rs 50000</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="pr-sec">
                        <div className="hd">
                            <div className="b-txt">
                                Facilities
                            </div>
                            <div className="ac">
                                <ShutterButton/>
                            </div>
                        </div>
                        <div className="bd">

                            <div className="row">
                                <div className="list hide-scroll">
                                    {
                                        facilities?.map((d) => {
                                            return (
                                                <CheckBox label={d}/>
                                            )
                                        })
                                    }
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div className="rt-cont">
                <div className="column">
                    <div className="header">
                        <div className="lft">
                            <div className="lft-group">
                                <button className="filter-btn">
                                    <div className="ic">
                                        <i className="ic-filter"/>
                                    </div>
                                    filters
                                </button>
                                <div className="header-text">
                                    <div className="txt">
                                        Results
                                    </div>
                                    <div className="sub-txt">
                                        {store.length} properties found
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="rt">
                            <div className="rt-group">
                                <div className="tabs">
                                    <div className="tab active">
                                        <div className="txt active">
                                            Our Top Pick
                                        </div>
                                    </div>
                                    <Divider type={"vertical"} style={{height: "100%"}}/>
                                    <div className="tab">
                                        <div className="txt">
                                            Price <span>(lowest first)</span>
                                        </div>
                                    </div>
                                    <Divider type={"vertical"} style={{height: "100%"}}/>
                                    <div className="tab">
                                        <div className="txt">
                                            Star Rating
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="section">
                        <div className="cards-holder hide-scroll">
                            {
                                store?.map((dt, index) => {
                                    return (
                                        <Card onSelect={onSelect} key={index} selected_id={selected_id} data={dt}/>
                                    )
                                })
                            }
                        </div>
                    </div>
                </div>

            </div>
        </div>
    );
};

