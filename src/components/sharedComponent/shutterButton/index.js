import React, {useState} from 'react';
import {DownOutlined} from "@ant-design/icons";
import {STRINGS} from "../../../utils/base";

const defaultState = {
    rotate: false,
}

const ShutterButton = (props) => {
    const [init, setInit] = useState(defaultState);
    let {rotate} = init;

    const handleClick = () => {
        setInit({
            ...init,
            rotate: !rotate
        })
    }

    return (
        <DownOutlined
            rotate={rotate ? 180 : 0}
            onClick={handleClick}
            size={24}
            style={{
                fontSize:"12px",
                color: `${STRINGS.COLORS.PRI}`
            }}/>
    );
};

export default ShutterButton;