import React, {useState} from 'react';
import {Switch} from "antd"; //ant design is a component lib which provide ready to use components for saving the development time

const defaultState = {
    disabled: false
}

const SwitchButton = () => {
    const [init, setInit] = useState(defaultState);
    let {disabled} = init;//destructuring the keys from defaultState  object

    const handleDisabledChange = () => {
        setInit({
            ...init,//spreading the remains keys which did not change in this set state
            disabled: !disabled
        })
    }

    return (
        <Switch size="small" checked={disabled} onChange={handleDisabledChange}/>
    );
};

export default SwitchButton;