import React from 'react';
import {Slider} from "antd";

const RangeSlider = (props) => {

    const onChange = () => {

    }
    const onAfterChange = () => {

    }

    return (
        <Slider
            range
            step={10}
            defaultValue={[20, 50]}
            onChange={onChange}
            onAfterChange={onAfterChange}
        />
    );
};

export default RangeSlider;