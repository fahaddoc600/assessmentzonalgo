import React from 'react';
import {Checkbox} from "antd";

const CheckBox = ({label}) => {
    const onChange = () => {

    }
    return (
        <Checkbox className={"chk"} onChange={onChange}>{label}</Checkbox>
    );
};

export default CheckBox;