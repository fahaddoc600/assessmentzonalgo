export const STRINGS = {
    COLORS: {
        PRI: "#32addf",
        TEXT: "#969c9d",
    }
}

export function priceWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}